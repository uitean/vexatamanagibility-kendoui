/*
 * @author: Subramanian M 
 * @param {Object} "layout"
 * @param {Object} {model:layoutModel}
 * @Version: Initial Version 0.0.1
 *   
 */

$(function(){
	
	layout = new kendo.Layout("layout", {model: layoutModel});
	var html = layout.render();
	$("body").html(html);

	dashBoard = new kendo.View("dashBorad", {model: dashBoardModel});
	layout.showIn("#page-wrapper", dashBoard);
	getDashCount();
	
	volViewModel = new kendo.data.ObservableObject({
		model: volumeModel,
		showDashBoard: function(e){
			e.preventDefault();
			layout.showIn("#page-wrapper", dashBoard);
			getDashCount();
		}
	});
	volumeGp = new kendo.View("volumeGroup", {model: volViewModel});
	
		
	layoutModel.bind("layout:dashBoard", function(){
		layout.showIn("#page-wrapper", dashBoard);
		getDashCount();
	});
	
		
	layoutModel.bind("user:volGroup", function(data){
				layout.showIn("#page-wrapper", volumeGp);
				$("#grid").kendoGrid({
						autoBind: false,
						toolbar: [{
							name: "create",
							messages: "+ Add new volume"
						}],
						dataSource: volumeModel,
						columns: [
							{ field:"ProductID", title: "Product Id" },
                            { field:"ProductName", title: "Product Name" },
                            { field: "UnitPrice", title:"Unit Price", format: "{0:c}", width: "120px" },
                            { field: "UnitsInStock", title:"Units In Stock", width: "120px" },
                            { field: "Discontinued", width: "120px" },
                            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }],
						editable: "popup",
						sortable: true,
						scrollable: false,
						pageable: true,
						groupable:{
							messages:{
								empty: "Drag and Drop Columns here"
							}
						}
			});
			volumeModel.read();
	});
	
	dashBoardModel.bind("dash:volGroup", function(data){
				layout.showIn("#page-wrapper", volumeGp);
				$("#grid").kendoGrid({
						autoBind: false,
						toolbar: [{
							name: "create",
							messages: "+ Add new volume"
						}],
						dataSource: volumeModel,
						columns: [
							{ field:"ProductID", title: "Product Id" },
                            { field:"ProductName", title: "Product Name" },
                            { field: "UnitPrice", title:"Unit Price", format: "{0:c}", width: "120px" },
                            { field: "UnitsInStock", title:"Units In Stock", width: "120px" },
                            { field: "Discontinued", width: "120px" },
                            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }],
						editable: "popup",
						sortable: true,
						scrollable: false,
						pageable: true,
						groupable:{
							messages:{
								empty: "Drag and Drop Columns here"
							}
						}
			});
			volumeModel.read();
	});

function getDashCount(){
	volumeModel.fetch(function(){
			$(".volumeGP").text(volumeModel.total());
	});
}
	
function createChart() {
        $("#chart").kendoChart({
            title: {
                text: "I/O Latency"
            },
            legend: {
                position: "bottom"
            },
            chartArea: {
                background: ""
            },
            seriesDefaults: {
                type: "line",
                style: "smooth"
            },
            series: [{
                name: "1 Thread",
                data: [10, 10, 11, 13, 17, 27, 43]
            },{
                name: "16 Thread",
                data: [35, 52, 64, 88, 133, 225, 303]
            }],
            valueAxis: {
                labels: {
                    format: "{0} &micro"
                },
                line: {
                    visible: true
                },
                axisCrossingValue: -10
            },
            categoryAxis: {
                categories: [.512, 4, 8, 16, 32, 128],
                majorGridLines: {
                    visible: true
                },
                labels: {
                    format: "{0} kb"
                }
            },
            tooltip: {
                visible: true,
                format: "{0}",
                template: "#= series.name #: #= value #"
            }
        });
    }

function createChartCapacity() {
        $("#chart-capacity").kendoChart({
            title: {
                position: "bottom",
                text: "Volume Capacity"
            },
            legend: {
				position: "top",
                visible: true
            },
            chartArea: {
                background: ""
            },
            seriesDefaults: {
                labels: {
                        template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                        //position: "outsideEnd",
                        visible: true,
                        background: "transparent"
                    }
            },
            series: [{
                type: "pie",
                startAngle: 150,
                data: [{
                    category: "Used",
                    value: 53.8,
                    color: "#428bca"
                },{
                    category: "Free",
                    value: 16.1,
                    color: "#5cb85c"
                },{
                    category: "Sanpshots",
                    value: 31.1,
                    color: "#d9534f"
                }]
            }],
            tooltip: {
                visible: true,
                format: "{0}%"
            }
        });
    }

function donutcreateChart() {
            $("#donut-chart").kendoChart({
                title: {
                    position: "bottom",
                    text: "Share of Internet Population Growth"
                },
                legend: {
                    visible: false
                },
                chartArea: {
                    background: ""
                },
                seriesDefaults: {
                    type: "donut",
                    startAngle: 150
                },
                series: [{
                    name: "2011",
                    data: [{
                        category: "Asia",
                        value: 30.8,
                        color: "#9de219"
                    },{
                        category: "Europe",
                        value: 21.1,
                        color: "#90cc38"
                    },{
                        category: "Latin America",
                        value: 16.3,
                        color: "#068c35"
                    },{
                        category: "Africa",
                        value: 17.6,
                        color: "#006634"
                    },{
                        category: "Middle East",
                        value: 9.2,
                        color: "#004d38"
                    },{
                        category: "North America",
                        value: 4.6,
                        color: "#033939"
                    }]
                }, {
                    name: "2012",
                    data: [{
                        category: "Asia",
                        value: 53.8,
                        color: "#9de219"
                    },{
                        category: "Europe",
                        value: 16.1,
                        color: "#90cc38"
                    },{
                        category: "Latin America",
                        value: 11.3,
                        color: "#068c35"
                    },{
                        category: "Africa",
                        value: 9.6,
                        color: "#006634"
                    },{
                        category: "Middle East",
                        value: 5.2,
                        color: "#004d38"
                    },{
                        category: "North America",
                        value: 3.6,
                        color: "#033939"
                    }],
                    labels: {
                        visible: true,
                        background: "transparent",
                        position: "outsideEnd",
                        template: "#= category #: \n #= value#%"
                    }
                }],
                tooltip: {
                    visible: true,
                    template: "#= category # (#= series.name #): #= value #%"
                }
            });
        }

$(document).ready(donutcreateChart);
$(document).bind("kendo:skinChange", donutcreateChart);

$(document).ready(createChart);
$(document).bind("kendo:skinChange", createChart);

$(document).ready(createChartCapacity);
$(document).bind("kendo:skinChange", createChartCapacity);
	
});