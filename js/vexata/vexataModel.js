/*
 * @author: Subramanian M 
 * @param {Object} "layout"
 * @param {Object} {model:layoutModel}
 * @Version: Initial Version 0.0.1
 *   
 */

$(function(){

	var userList = []; //Empty List mock data
	var crudServiceBaseUrl = "http://demos.telerik.com/kendo-ui/service";
	
	volumeModel = new kendo.data.DataSource({
			transport: {
                        read:  {
                            url: crudServiceBaseUrl + "/Products",
                            dataType: "jsonp"
                        },
                        update: {
                            url: crudServiceBaseUrl + "/Products/Update",
                            dataType: "jsonp"
                        },
                        destroy: {
                            url: crudServiceBaseUrl + "/Products/Destroy",
                            dataType: "jsonp"
                        },
                        create: {
                            url: crudServiceBaseUrl + "/Products/Create",
                            dataType: "jsonp"
                        },
                        parameterMap: function(options, operation) {
                            if (operation !== "read" && options.models) {
                                return {models: kendo.stringify(options.models)};
                            }
                        }
                    },
                    batch: true,
					schema: {
                        model: {
                            id: "ProductID",
                            fields: {
                                ProductID: { editable: false, nullable: true },
                                ProductName: { validation: { required: true } },
                                UnitPrice: { type: "number", validation: { required: true, min: 1} },
                                Discontinued: { type: "boolean" },
                                UnitsInStock: { type: "number", validation: { min: 0, required: true } }
                            }
                        }
                },
			pageSize: 10
	});
	
	layoutModel = kendo.observable({
		volumeGroup: function(e){
			e.preventDefault();
			this.trigger("user:volGroup");
		},
		showDashBoard: function(e){
			e.preventDefault();
			this.trigger("layout:dashBoard");
		},
	});
	
	dashBoardModel = kendo.observable({
		count: "..",
		volumeGroup: function(e){
			e.preventDefault();
			this.trigger("dash:volGroup");
		},
	});
	

function clearList(obj){
			obj.set("name", "");
			obj.set("desg", "");
			obj.set("url", "images/user.png");
			obj.set("usrId", "udefined");
	}	    
});